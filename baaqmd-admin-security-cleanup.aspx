<%@ Page language="c#" AutoEventWireup="true" %>
<%@ Import Namespace="System.Text" %>
<%@ Import Namespace="Sitecore.StringExtensions" %>
<%@ Import Namespace="Sitecore.Diagnostics" %>
<%@ Import Namespace="Sitecore.Data.Items" %>
<%@ Import Namespace="Sitecore.Security.Accounts" %>

<script runat="server">
    protected enum ScriptMode {
        DryRun,
        Apply
    }

    protected class ExecutionStats {
        public int SkippedCount { get; set; }
        public int ErrorsCount { get; set; }
        public List<String> ItemsProcessed {get; private set; }

        public ExecutionStats() {
            this.SkippedCount = 0;
            this.ErrorsCount = 0;
            this.ItemsProcessed = new List<String>();
        }

        public int ProcessedCount {
            get {
                return ItemsProcessed.Count + SkippedCount + ErrorsCount;
            }
        }
    }

    // SCRIPT PAYLOAD
    protected ExecutionStats Run_Script(ScriptMode mode, Item rootItem) {
        //Scan DB recursively
        //Keep list of processed, count of skipped
        var accountToRemove = Account.FromName("rs\\BAAQMD Admin", AccountType.Role);
        var stats = new ExecutionStats();
        Execute_Recursively(mode, rootItem, accountToRemove, ref stats);
        return stats;
    }

    protected void Execute_Recursively(ScriptMode mode, Item rootItem, Account accountToRemove, ref ExecutionStats stats) {
        //Log Count
        //Process item
        //Handle exception
        //Update count
        try {
            if ((stats.ProcessedCount % 100) == 0) {
                Log.Info("[BASC][CNT]: {0} items processed".FormatWith(stats.ProcessedCount), this);
            }
            Log.Info("[BASC][PRG]: {0}".FormatWith(rootItem.Paths.Path), this);
            var rules = rootItem.Security.GetAccessRules();
            var has_account = rules.Helper.ContainsAccount(accountToRemove);
            if (has_account) {
                if (mode == ScriptMode.Apply) {
                    rules.Helper.RemoveExactMatches(accountToRemove);
                    rootItem.Security.SetAccessRules(rules);
                } 
                stats.ItemsProcessed.Add(rootItem.Paths.Path);
            } else {
                stats.SkippedCount += 1;
            }
        } catch (Exception ex) {
            stats.ErrorsCount += 1;
            Log.Error("[BASC][ERR]", ex, this);
        }
        //Recurse
        foreach (Item child_item in rootItem.Children) {
            Execute_Recursively(mode, child_item, accountToRemove, ref stats);
        }
    }

    // HELPER FUNCTIONS
    protected string Get_Execution_Summary(ExecutionStats stats, ScriptMode mode){
        var summary_msg = new StringBuilder("<pre><code>");
        if (mode == ScriptMode.Apply) {
            summary_msg.AppendLine(" ******* SCRIPT EXECUTION COMPLETE *******");
        } else {
            summary_msg.AppendLine(" ~ scan complete ~");
        }
        summary_msg.AppendLine("Execution Summary:\n");
        summary_msg.AppendLine("Scanned Items ( total ): {0}".FormatWith(stats.ProcessedCount));
        summary_msg.AppendLine((mode == ScriptMode.Apply ? "Changed Items": "Items to change")+": {0}".FormatWith(stats.ItemsProcessed.Count));
        summary_msg.AppendLine("Ignored Items ( total ): {0}".FormatWith(stats.SkippedCount));
        summary_msg.AppendLine("Errors Encountered ( none expected ): {0}".FormatWith(stats.ErrorsCount));
        summary_msg.AppendLine("--- List of " + ((mode == ScriptMode.Apply) ? "modified items": "items to modify:"));
        foreach (var entry in stats.ItemsProcessed) {
            summary_msg.AppendLine(entry);
        }
        if (stats.ItemsProcessed.Count == 0) {
            summary_msg.AppendLine("   *** EMPTY ***");
        }
        summary_msg.AppendLine("</code></pre>");
        return summary_msg.ToString();
    }

    // PAGE EVENT HANDLERS
    protected void Go_Click(object sender, EventArgs e) {
        try {
            if (!Sitecore.Context.User.IsAdministrator) {
                throw new System.Security.SecurityException("Current user does not have Sitecore administrator privileges to run the script.");
            }
            var db = list_targetdb.SelectedItem.Value;
            var action = list_action.SelectedItem.Value;

            var sc_db = Sitecore.Configuration.Factory.GetDatabase(db);
            ScriptMode mode = (action == "apply") ? ScriptMode.Apply : ScriptMode.DryRun;

            var stats = Run_Script(mode, sc_db.SitecoreItem);
            summary.Text = Get_Execution_Summary(stats, mode);
        } catch ( Exception ex ) {
            var ex_msg = new StringBuilder("<pre><code>EXCEPTION RUNNING THE SCRIPT:\n");
            ex_msg.AppendLine(ex.Message);
            ex_msg.AppendLine(ex.StackTrace);
            ex_msg.AppendLine("</code></pre>");
            summary.Text = ex_msg.ToString();
        }
    }
    
    public void Page_Load() {
        if (!IsPostBack) {
            var db_list = Sitecore.Configuration.Factory.GetDatabaseNames();
            foreach (var db_name in db_list) {
                list_targetdb.Items.Add(new ListItem(db_name, db_name));
            }
        }
    }
    
</script>

<!DOCTYPE html>
<HTML>
  <HEAD>
    <title>BAAAMD Admin Security Cleanup</title>
  </HEAD>
<body>
<div>
<h2>IMPORTANT</h2>
<ul>
<li>It is highly recommended to create backups of the databases to be processed with the script</li>
<li>The script scans the whole database so it is advised to run it during low activity time period for the Sitecore instance in question</li>
</ul>
<ul>
 <li>The script requires admin privileges to be executed</li>
 <li>The script recursively scans all database items and removes all explicit security settings for the <strong>rs\BAAQMD Admin</strong> role</li>
 <li>Upon completion the script will provide the summary of execution ( number of items scanned, processed / to be processed )</li>
 <li>The progress of the execution can be monitored via active Sitecore log file using '[BASC][PRG]' ( item being processed ) or '[BASC][CNT]' ( number of items processed ) entries</li>
</ul> 
</div>
<hr />
<form id=Form1 method=post runat="server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <span>Apply the script to the</span>
    <asp:dropdownlist id="list_targetdb" runat="server"/>
    <span>database and</span>
    <asp:dropdownlist id="list_action" runat="server">
        <asp:ListItem Enabled="true" Text="Scan for stats without changing database" Value="dryrun" />
        <asp:ListItem Text="Remove assignments and save changes to database" Value="apply" />
    </asp:dropdownlist>
    <asp:button id="go" runat="server" Text="GO" onclick="Go_Click"></asp:button>
    <br/><asp:Label id="summary" runat="server" />
</FORM>
</body>
</HTML>

